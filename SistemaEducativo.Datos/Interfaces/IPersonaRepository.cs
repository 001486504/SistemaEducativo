﻿using SistemaEducativo.Entidades;
using System.Collections.Generic;

namespace SistemaEducativo.Datos.Interfaces
{
    public interface IPersonaRepository
    {
        Persona ObtenerPorDni(string dni);
        IEnumerable<Persona> ObtenerTodas();
        void Insertar(Persona persona);
        void Actualizar(Persona persona);
        void Eliminar(string dni);
    }
}
