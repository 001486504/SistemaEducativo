﻿using SistemaEducativo.Datos.Interfaces;
using SistemaEducativo.Entidades;
using SistemaEducativo.Negocio.Interfaces;
using System;
using System.Collections.Generic;

namespace SistemaEducativo.Negocio
{
    public class PersonaService : IPersonaService
    {
        private readonly IPersonaRepository _personaRepository;

        public PersonaService(IPersonaRepository personaRepository)
        {
            _personaRepository = personaRepository ?? throw new ArgumentNullException(nameof(personaRepository));
        }

        public Persona ObtenerPersonaPorDni(string dni)
        {
            return _personaRepository.ObtenerPorDni(dni);
        }

        public IEnumerable<Persona> ObtenerTodasLasPersonas()
        {
            return _personaRepository.ObtenerTodas();
        }

        public void InsertarPersona(Persona persona)
        {
            // Aquí puedes realizar validaciones u otras operaciones de negocio antes de insertar
            _personaRepository.Insertar(persona);
        }

        public void ActualizarPersona(Persona persona)
        {
            // Aquí puedes realizar validaciones u otras operaciones de negocio antes de actualizar
            _personaRepository.Actualizar(persona);
        }

        public void EliminarPersonaPorDni(string dni)
        {
            // Aquí puedes realizar validaciones u otras operaciones de negocio antes de eliminar
            _personaRepository.Eliminar(dni);
        }
    }
}
